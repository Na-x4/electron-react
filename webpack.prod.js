const { merge } = require("webpack-merge");
const common = require("./webpack.common.js");

const main = {
  mode: "production",
};

const preload = {
  mode: "production",
};

const renderer = {
  mode: "production",
  devtool: "source-map",
};

module.exports = [main, preload, renderer].map((prod, i) =>
  merge(common[i], prod)
);
