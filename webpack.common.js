const path = require("path");

const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const outputPath = path.join(__dirname, "app");

const main = {
  mode: "development",
  target: "electron-main",
  entry: "./src/main/index",
  node: false,
  output: {
    filename: "main.js",
    path: outputPath,
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        loader: "ts-loader",
      },
    ],
  },
  resolve: {
    extensions: [".js", ".ts"],
  },
};

const preload = {
  mode: "development",
  target: "electron-preload",
  entry: "./src/preload/index",
  node: false,
  output: {
    filename: "preload.js",
    path: outputPath,
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        loader: "ts-loader",
      },
    ],
  },
  resolve: {
    extensions: [".js", ".ts"],
  },
};

const renderer = {
  mode: "development",
  devtool: "inline-source-map",
  entry: ["./src/renderer/index", "./src/renderer/style.css"],
  output: {
    filename: "index.js",
    path: path.join(outputPath, "renderer"),
  },
  resolve: {
    extensions: [".js", ".jsx", ".ts", ".tsx"],
  },
  module: {
    rules: [
      {
        test: /\.(tsx|ts)$/,
        use: ["ts-loader"],
      },
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, "css-loader"],
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          {
            loader: "url-loader",
            options: {
              limit: 10000,
              fallback: "file-loader",
              name: "fonts/[name].[ext]",
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "style.css",
    }),
    new HtmlWebpackPlugin({
      title: "Starting...",
      meta: {
        "Content-Security-Policy": {
          "http-equiv": "Content-Security-Policy",
          content: "script-src 'self'",
        },
      },
    }),
  ],
};

module.exports = [main, preload, renderer];
