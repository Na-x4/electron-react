import React from "react";

import { IconButton } from "@material-ui/core";
import { DeveloperMode as DevToolsIcon } from "@material-ui/icons";

import { canUseDevTools, openDevTools } from "./devTools";

export default () => {
  return (
    <>
      <h1>Hello, world!!</h1>
      {canUseDevTools() && (
        <IconButton onClick={openDevTools}>
          <DevToolsIcon />
        </IconButton>
      )}
    </>
  );
};
