export interface DevTools {
  open: () => void;
  close: () => void;
}

declare const devTools: DevTools;

export function canUseDevTools() {
  return typeof devTools !== "undefined" && !!devTools.open && !!devTools.close;
}

export function openDevTools() {
  if (canUseDevTools()) {
    devTools.open();
  }
}

export function closeDevTools() {
  if (canUseDevTools()) {
    devTools.close();
  }
}
