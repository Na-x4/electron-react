import ReactDOM from "react-dom";
import React from "react";

import App from "./App";

export function initialize() {
  const appElement = document.createElement("div");
  appElement.id = "app";
  document.body.append(appElement);
  ReactDOM.render(<App />, appElement);
}
