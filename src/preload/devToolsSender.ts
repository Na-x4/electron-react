import { ipcRenderer } from "electron";
import { DevTools } from "../renderer/devTools";

export const devToolsSender: DevTools = {
  open: () => {
    ipcRenderer.send("devTools", "open");
  },
  close: () => {
    ipcRenderer.send("devTools", "close");
  },
};
