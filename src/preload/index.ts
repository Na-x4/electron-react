import { devToolsSender } from "./devToolsSender";
import { contextBridge } from "electron";

contextBridge.exposeInMainWorld("devTools", devToolsSender);
