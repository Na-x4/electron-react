import { app, ipcMain } from "electron";

import { blockAllNavigation } from "./blockAllNavigation";
import { devToolsListener } from "./devToolsListerner";
import { createWindow } from "./createWindow";

blockAllNavigation(app);
ipcMain.on("devTools", devToolsListener);
app.whenReady().then(createWindow);
