import { App } from "electron";

export function blockAllNavigation(app: App) {
  app.on("web-contents-created", (_event, contents) => {
    contents.on("will-navigate", (event, navigationURL) => {
      console.warn(
        `The application tried to navigate to "${navigationURL}". But it was blocked.`
      );
      event.preventDefault();
    });
    contents.on("new-window", (event, navigationURL) => {
      const url = new URL(navigationURL);
      if (url.protocol === "devtools:") {
        return;
      }
      console.warn(
        `The application tried to open a window to "${navigationURL}". But it was blocked.`
      );
      event.preventDefault();
    });
  });
}
