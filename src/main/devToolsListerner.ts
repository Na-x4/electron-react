import { IpcMainEvent } from "electron";

export function devToolsListener(
  event: IpcMainEvent,
  message: "open" | "close"
) {
  switch (message) {
    case "open":
      event.sender.openDevTools({ mode: "undocked" });
      break;

    case "close":
      event.sender.closeDevTools();
      break;
  }
}
