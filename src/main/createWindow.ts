import { app, BrowserWindow } from "electron";
import { join as joinPath } from "path";

export function createWindow() {
  let win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      contextIsolation: true,
      enableRemoteModule: false,
      preload: joinPath(app.getAppPath(), "app/preload.js"),
    },
  });

  win.loadFile("app/renderer/index.html");
}
