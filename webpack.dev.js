const { merge } = require("webpack-merge");
const common = require("./webpack.common.js");

const main = {
  mode: "development",
  devtool: "cheap-module-source-map",
};

const preload = {
  mode: "development",
};

const renderer = {
  mode: "development",
  devtool: "inline-source-map",
};

module.exports = [main, preload, renderer].map((dev, i) =>
  merge(common[i], dev)
);
